#!/bin/bash
if [ ! -d "notes" ]; then
  echo "no notes folder. pulling latest notes"
  git clone git@bitbucket.org:haitechdev/django-2-react-build-a-realtime-web-app.git notes
  cd notes/frontend
  npm install
fi

docker-compose up